#!/usr/bin/make -f

export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/buildflags.mk
include /usr/share/debhelper/dh-fortran/fortran-support.mk

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
LIBDIR:=/usr/lib/$(DEB_HOST_MULTIARCH)
INCDIR:=/usr/include/$(DEB_HOST_MULTIARCH)
INSTDIR:=$(CURDIR)/debian/tmp
CMAKE_DIR:=$(INSTDIR)/$(LIBDIR)/cmake
FMODDIR:= $(call get_fmoddir,$(FC_DEFAULT))

export HIPCXX=clang++-17

DEB_CFLAGS_MAINT_APPEND= -I$(CURDIR)/src -I$(CURDIR)/obj-$(DEB_HOST_MULTIARCH)/src
DEB_CXXFLAGS_MAINT_APPEND= -I$(CURDIR)/src -I$(CURDIR)/obj-$(DEB_HOST_MULTIARCH)/src
DEB_FFLAGS_MAINT_APPEND= -I$(FMODDIR)

#
# OMP Doesn't appear to work on these archs
ARCH_NO_OMP:= riscv64 sparc64
ENABLE_OMP:=  $(if $(filter $(DEB_TARGET_ARCH), $(ARCH_NO_OMP)),Off,On)

ARCH_HIP:= amd64 arm64 ppc64el
ENABLE_HIP:= $(if $(filter $(DEB_TARGET_ARCH), $(ARCH_HIP)),On,Off)
# HIP options thanks to Cory Bloor: match the math libs ectrans depends on
# "ACC" == HIP, "GPU" == "CUDA"

BUILD_FLAGS = \
	-DCMAKE_HIP_ARCHITECTURES="gfx803;gfx900;gfx906;gfx908;gfx90a;gfx1010;gfx1030;gfx1100;gfx1101;gfx1102"  \
	-DENABLE_GPU=Off \
	-DENABLE_MKL=Off \
	-DENABLE_ACC=$(ENABLE_HIP) \
	-DENABLE_MPI=On \
	-DENABLE_OMP=$(ENABLE_OMP) 

# Test suite requires 4 processors. Fake it
export OMPI_MCA_rmaps_base_oversubscribe=1

%:
	dh $@ --buildsystem=ecbuild --builddir=debian/build --with fortran_mod

override_dh_auto_configure:
	dh_auto_configure -- ${BUILD_FLAGS} 

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	LD_LIBRARY_PATH=$(CURDIR)/debian/build/lib dh_auto_test || true
endif

override_dh_auto_install:
	dh_auto_install
	chrpath -d $(INSTDIR)/usr/bin/ectrans-benchmark*
	chrpath -d $(INSTDIR)/$(LIBDIR)/*.so*
	# Fix broken paths
	sed -i -e 's%$${_IMPORT_PREFIX}/include/ectrans%/usr/include/${DEB_HOST_MULTIARCH}/ectrans%' \
		$(CMAKE_DIR)/ectrans/ectrans-targets.cmake	
	# Don't ship symlinks

override_dh_fixperms:
	dh_fixperms
	rm debian/libectrans-dev/$(INCDIR)/ectrans/transi.h debian/libectrans-dev/$(INCDIR)/ectrans/version.h
	cp src/transi/version.h src/transi/transi.h debian/libectrans-dev/$(INCDIR)/ectrans 
