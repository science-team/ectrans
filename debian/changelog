ectrans (1.5.1-1) unstable; urgency=medium

  * New upstream release
  * Drop unused config vars ENABLE_DOCS, ENABLE_FORTRAN, ENABLE_FFTW
  * Drop ia64 references in d/control; no  longer suppported
  * Drop B-D on dh-buildinfo

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 20 Dec 2024 16:04:30 +0000

ectrans (1.5.0-3) unstable; urgency=medium

  * Set archs for HIP to re-enable other archs

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 18 Nov 2024 17:14:13 +0000

ectrans (1.5.0-2) unstable; urgency=medium

  * Build against openmpi >= 5.0.5-6. Closes: #1086263
  * Experimental HIP support:
    - build-dep on hipcc, librocsolver-dev, libhipblas-dev, libhipfft-dev
      librocfft-dev,
    - set DH_VERBOSE=1
    - set ENABLE_ACC
    - Set CMAKE_HIP_ARCHITECTURES to match math lib supported archs
  * gfortran-fixes.patch: fixes needed for compilation

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 17 Nov 2024 06:08:38 +0000

ectrans (1.5.0-1) unstable; urgency=medium

  * New upastream release
  * Fix typo in homepage in d/control
  * Drop hard-coded dep on libopenmpi-dev

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 12 Oct 2024 06:37:33 +0100

ectrans (1.4.0-1) unstable; urgency=medium

  * New upstream release
    - update patches for new libraries
  * Add loongarch64 support. Closes: #1059601
  * Module directory name changed trans -> ectrans

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 01 Aug 2024 11:06:45 +0100

ectrans (1.3.2-1) unstable; urgency=medium

  * New upstream release
  * Set Debian Science Maint. as maintainer
  * Standards-Version: 4.7.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 25 Apr 2024 09:27:41 +0100

ectrans (1.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 12 Apr 2024 08:48:43 +0100

ectrans (1.3.0-2) unstable; urgency=medium

  * Ship real header files; build shipped broken symlinks. Closes: #1066427

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 13 Mar 2024 16:08:43 +0000

ectrans (1.3.0-1) unstable; urgency=medium

  * New upstream release
  * Drop strip() hack; bug fixed

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 02 Mar 2024 10:20:50 +0000

ectrans (1.2.0-2) unstable; urgency=medium

  * Standards-Version: 4.6.2
  * Build-dep on architecture-is-64-bit
  * Update to arch: any rather than specified list
  * Update openblas,lapack deps
  * Build-depend on dh-fortran-mod (>= 0.32)

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 02 Jan 2024 18:26:49 +0000

ectrans (1.2.0-1) experimental; urgency=medium

  * Add d/watch file
  * New upstream release
  * Ignore test failures; just log in this release
  * Add bash-completion file

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 30 Jan 2023 15:56:13 +0000

ectrans (1.1.0-4) unstable; urgency=medium

  * Enable tests

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 23 Jan 2023 15:51:22 +0000

ectrans (1.1.0-3) unstable; urgency=medium

  * Add patches from Vagrant Cascadian for reproducibility. Closes: #1029227
    - 0001-src-programs-ectrans.in-Avoid-embedding-the-running-.patch
    - 0002-Pass-BUILD_TIMESTAMP-via-CMakeLists.txt-and-use-in-e.patches

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 20 Jan 2023 15:45:54 +0000

ectrans (1.1.0-2) unstable; urgency=medium

  * Fix cmake ectrans-targets.cmake paths
  * Enable MPI

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 24 Nov 2022 14:20:39 +0000

ectrans (1.1.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1024552)
    Additional copyrights added

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 19 Nov 2022 12:39:17 +0000
